﻿using System;
using System.Management;

namespace DFOVC
{
    public class ProcessMonitoring : IProcessMonitoring
    {
        private static int mode;
        private static bool flag = false, started = false;
        private ManagementEventWatcher watcher;
        private string query;
        private static string report, reportOther;

        string IProcessMonitoring.MonitorReport => report;
         bool IProcessMonitoring.Detected => flag;
        string IProcessMonitoring.OtherReport => reportOther;
        bool IProcessMonitoring.Started => started;

        public ProcessMonitoring(string AppName, int Mode)
        {
            query =
            "SELECT * FROM __InstanceOperationEvent "
            + "WITHIN 2 WHERE " +
            "TargetInstance ISA 'Win32_Process' AND TargetInstance.Name = '" + AppName + "'";//WQL Query to monitor the progress.
            watcher = new ManagementEventWatcher(query);
            watcher.EventArrived += new EventArrivedEventHandler(OnEventArrived);
            mode = Mode;
        }

        void IProcessMonitoring.Start()
        {
            watcher.Start();
            started = true;
            report = "Monitoring the process";
        }

        public void Stop()
        {
            watcher.Stop();
            started = false;
            report = "Monitoring stopped";
        }

        private static void OnEventArrived(object sender, EventArrivedEventArgs e)
        {
            string str = String.Empty, rpt = String.Empty;
            switch (mode)//Select which event to monitor.
            {
                case 0:
                    str = "InstanceCreationEvent";
                    rpt = "creation";
                    break;
                case 1:
                    str = "InstanceDeletionEvent";
                    rpt = "deletion";
                    break;
                case 2:
                    str = "InstanceModificationEvent";
                    rpt = "modification";
                    break;
            }
            if (e.NewEvent.ClassPath.ClassName.Contains(str))
            {
                report = "Process " + rpt + " detected.";
                flag = true;
            }
            else
                reportOther = e.NewEvent.ClassPath.ClassName;//Return the name of event detected.
        }
    }
}

public interface  IProcessMonitoring
{
    string MonitorReport { get; }
    bool Detected { get; }
    string OtherReport { get; }
    bool Started { get; }
    void Start();
    void Stop();
}