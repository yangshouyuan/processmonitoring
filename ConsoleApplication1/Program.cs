﻿using System;
using System.Diagnostics;
using System.IO;
using System.ComponentModel;

namespace DFOVC
{
    class Program
    {
        static IProcessMonitoring monitor;
        static BackgroundWorker back;
        static ConsoleKey key,
                    enter = ConsoleKey.Enter,
                    esc = ConsoleKey.Escape;                                       

        static void Main()
        {
            try
            {
                monitor = new ProcessMonitoring("NeopleLauncher.exe", 1);
                //monitor.Initialize("NeopleLauncher.exe", 1);//Name of the process being monitored.
                back = new BackgroundWorker();
                back.DoWork += OnDoWork;//Hook to the event.
                fileRaplace(0);//Replace audio.xml with audioOriginal.xml and backup audio.xml as audioMod.xml.
                Console.WriteLine("Success.\nStarting the game...");
                Process.Start(fileName: "steam://rungameid/495910");//Start the game.
                Console.WriteLine("Success.");
                monitor.Start();//Start to monitor the process.
                Console.WriteLine("{0}...\nPress ESC to cancel.", monitor.MonitorReport);//Print monitoring progress.
                back.RunWorkerAsync();//Run the background work.
                while (!monitor.Detected && monitor.Started) ;//wait for deletion detection or monitoring cancellation.
                if (!monitor.Started)//If monitoring is cancelled, exit the application.
                    return;
                Console.WriteLine(monitor.MonitorReport);
                monitor.Stop();//Stop monitoring and break out the while loop waiting for ESC to be pressed.
                System.Threading.Thread.Sleep(2000);
                fileRaplace(1);//Replace audio.xml with audioMod.xml and backup audio.xml as audioOriginal.xml.
                Console.WriteLine("Success.\nExiting in 2 seconds...");
                System.Threading.Thread.Sleep(2000);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Files to be replaced not found.\nPress Enter to retry or press ESC to exit.");
                while ((key = Console.ReadKey(true).Key) != enter && key != esc) ;//Wating for Enter or ESC pressed.
                if (key == enter)//If Enter pressed, restart the application. If ESC pressed, exit the application.
                    Main();
            }
            catch (Exception)
            {
                Console.WriteLine("An error occured.\nPress Enter to retry or press ESC to exit.");
                while ((key = Console.ReadKey(true).Key) != enter && key != esc) ;//Wating for Enter or ESC pressed.
                if (key == enter)//If Enter pressed, restart the application. If ESC pressed, exit the application.
                    Main();
            }
        }

        static void OnDoWork(object sender, DoWorkEventArgs e)//Check if Home Key is pressed in another thread when waiting for deletion detection.
        {
            while (Console.ReadKey(true).Key != esc && monitor.Started) ;//Wait for ESC pressed or monitoring stopped.
            fileRaplace(1);//Replace audio.xml with audioMod.xml and backup audio.xml as audioOriginal.xml.
            monitor.Stop();//Cancel monitoring and break out the while loop waiting for process deletion detection.
        }

        private static void fileRaplace(int i)
        {
            Console.WriteLine("Replacing files...");
            if (i == 0 && !(File.Exists(".\\audioMod.xml") && File.Exists(".\\audio.xml")))
                File.Replace(".\\audioOriginal.xml", ".\\audio.xml", ".\\audioMod.xml");
            else if (i == 1 && !(File.Exists(".\\audioOriginal.xml") && File.Exists(".\\audio.xml")))
                File.Replace(".\\audioMod.xml", ".\\audio.xml", ".\\audioOriginal.xml");
            else
                Console.WriteLine("Files are already replaced.");
        }
    }
}
